

function Ball(context, init_x, init_y){	
	this.GRAVITY = 9.86;
	this.RESTITUTION = 0.8;
	this.INIT_VELOCITY = 50;
	this.RADIUS = 14.0;
	this.BORDER = 0.0;
	
	this.context = ctx;
	
	this.x = init_x;
	this.y = init_y;
	
	this.ax = 0.0;
    this.ay = this.GRAVITY;
	
	this.vx = this.randomVelocity();
    this.vy = this.randomVelocity();

	this.color = this.randomColor();
	


}

Ball.prototype.tick = function(delta) {
	// Update velocity and position.
	
    this.vx += this.ax * delta;
    this.vy += this.ay * delta;

    this.x += this.vx * delta;
    this.y += this.vy * delta;

    // Handle falling off the edge.
    if ((this.x < this.RADIUS) || (this.x > clientWidth())) {
      return false;
    }

    // Handle ground collisions.
    if (this.y >= clientHeight() - this.RADIUS - this.BORDER) {
      this.y = clientHeight() - this.RADIUS - this.BORDER;
      this.vy *= -this.RESTITUTION;
    }

    // Position the element.
	this.draw();
    return true;
}

Ball.prototype.randomVelocity = function() {
  return (Math.random() - 0.5) * this.INIT_VELOCITY;
}

Ball.prototype.randomColor = function() {
	colors = new Array('Orange', 'Red', 'Green', 'Yellow', 'Blue');
	return colors[Math.floor(Math.random() * (colors.length + 1))];
}

Ball.prototype.draw = function(){
	ctx.fillStyle = this.color;
	ctx.beginPath();
	ctx.arc(this.x, this.y, this.RADIUS, 0, Math.PI*2, true); 
	ctx.closePath();
	ctx.fill();
}