function Balls(context){
	this.RADIUS2 = Ball.RADIUS * Ball.RADIUS;
	this.lastTime = currentTimeMillis();	
	this.context = ctx;
	this.balls = new Array();
	for(var i = 0; i < 50; i++){
		this.balls.push(new Ball(ctx, clientWidth()/2, 10));
	}
}

Balls.prototype.tick = function(){
	now = currentTimeMillis();
	//delta = Math.min((now - this.lastTime) / 10.0, 0.25);
	this.lastTime = now;
	//draw balls
	//console.debug(delta);
	for(b in this.balls){
		this.balls[b].tick(0.2);	
	}
	//this.collideBalls(0.2);
}

Balls.prototype.collideBalls = function(delta) {
	// TODO: Make this nasty O(n^2) stuff better.
	for(var b0 = 0; b0 < this.balls.length; b0++){
		for(var b1 = 0; b1 < this.balls.length; b1++){
			// See if the two balls are intersecting.			
			dx = Math.abs(this.balls[b0].x - this.balls[b1].x);
		    dy = Math.abs(this.balls[b0].y - this.balls[b1].y);
			d2 = dx * dx + dy * dy;
			console.debug(d2);
			// Make sure they're actually on a collision path
	        // (not intersecting while moving apart).
	        // This keeps balls that end up intersecting from getting stuck
	        // without all the complexity of keeping them strictly separated.
			if(d2 < this.RADIUS2){
				if (this.newDistanceSquared(delta, this.balls[b0], this.balls[b1]) > d2) {
		          continue;
		        }
			}
			
			// They've collided. Normalize the collision vector.
	        d = Math.sqrt(d2);
	        if (d == 0) {
	          // TODO: move balls apart.
	          continue;
	        }
	        dx /= d;
	        dy /= d;
	
			// Calculate the impact velocity and speed along the collision vector.
	        impactx = this.balls[b0].vx - this.balls[b1].vx;
	        impacty = this.balls[b0].vy - this.balls[b1].vy;
	        impactSpeed = impactx * dx + impacty * dy;

	        // Bump.
	        this.balls[b0].vx -= dx * impactSpeed;
	        this.balls[b0].vy -= dy * impactSpeed;
	        this.balls[b1].vx += dx * impactSpeed;
	        this.balls[b1].vy += dy * impactSpeed;
		}
	}
}

Balls.prototype.newDistanceSquared = function(delta, b0, b1) {
  nb0x = b0.x + b0.vx * delta;
  nb0y = b0.y + b0.vy * delta;
  nb1x = b1.x + b1.vx * delta;
  nb1y = b1.y + b1.vy * delta;
  ndx = Math.abs(nb0x - nb1x);
  ndy = Math.abs(nb0y - nb1y);
  nd2 = ndx * ndx + ndy * ndy;
  return nd2;
}