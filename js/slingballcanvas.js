var canvasname = "canvas";
var ctx;
var prev_time;
var prev_disp_time;
var fps;
var balls;

/*
* Animate the canvas at an acceptable rate for the browser
*/
window.requestAnimFrame = (function(callback){
    return window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback){
        window.setTimeout(callback, 1000 / 60);
    };
})();
 
function animate(){ 
    draw();
    requestAnimFrame(function(){
        animate();
    });
}

/*
* Init canvas and start animation
*/
function initCanvas(){
    canvas = document.getElementById(canvasname);
    if(canvas.getContext("2d")){
		ctx = canvas.getContext("2d");
		fps = 1;
		var d = new Date();
		prev_time = d.getTime();
		var d = new Date();
		prev_disp_time = d.getTime();
		
		//for(var i = 0; i < 500; i++){
		//	balls.push(new Ball(ctx, clientWidth()/2, 10));
		//}
		balls = new Balls(ctx);
		
		animate();
    }
}

/*
*	Main drawing function
*/
function draw() {
  	ctx.canvas.width  = clientWidth();
  	ctx.canvas.height = clientHeight();
	
	//draw background
	ctx.beginPath();
    ctx.strokeStyle = 'rgb(237,239,244)';
	ctx.fillStyle = "rgb(237,239,244)";
    ctx.rect(0,0,ctx.canvas.width,ctx.canvas.height);
    ctx.closePath();
    ctx.fill();

	//draw balls
	balls.tick();
	//for(b in balls){
	//	balls[b].tick(0.25);	
	//}
	
	//draw debug info
	drawDebugInfo();
}

function drawDebugInfo(){
	var d = new Date();
	cur_time = d.getTime();
	ctx.fillStyle = "Black";
	if(parseInt(cur_time - prev_disp_time,10) >= 250){
		fps = 1000 * (1/(cur_time - prev_time));
		var d = new Date();
		prev_disp_time = d.getTime();
	}
	ctx.fillText(fps.toPrecision(3) + " FPS", ctx.canvas.width - 50, 10);
	
	var d = new Date();
	prev_time = d.getTime();
}