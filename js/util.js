function clientWidth() {
	return window.innerWidth;
}

function clientHeight() {
	return window.innerHeight;
}

function currentTimeMillis() {
   return (new Date).getTime();
 }